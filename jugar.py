# -*- coding: utf-8 -*-
from barcos import *
from configuracion import *

init_window()

##### ELEGIR IDIOMA ####
# Cargamos el idioma por defecto por si el idioma seleccionado no tiene 
# todas las cadenas definidas
cargar_idioma('es')
idioma = elegir_idioma()
cargar_idioma(idioma)

configurar()
seguir_jugando = True

while seguir_jugando:
    ##### INTRO ####
    intro()

    ##### COLOCA LOS BARCOS #####
    tablero1, posiciones_barcos1 = colocar_barcos()
#    tablero1, posiciones_barcos1 = ordenador_coloca_barcos(tipos_barcos)
    tablero2, posiciones_barcos2 = ordenador_coloca_barcos(tipos_barcos)
    print()
    pprint(tablero1)
    print(posiciones_barcos1)
    pprint(tablero2)
    print(posiciones_barcos2)

    ##### INTRO DISPARAR####
    intro_jugar()

    ##### DISPARAR #####
    jugar(tablero1, tablero2, posiciones_barcos1, posiciones_barcos2)

    ##### JUGAR OTRA VEZ? #####
    seguir_jugando = volver_a_jugar()

creditos()
