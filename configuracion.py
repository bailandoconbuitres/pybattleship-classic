# -*- coding: utf-8 -*-

tipos_barcos = [2,3,3,4]
lado = 8

letras = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

WHITE = (255,255,255)
GREY = (100,100,100)
BLACK = (0,0,0)
RED = (255,0,0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

PEQUEÑO = {
    'LADO_CUADRADO': 30,
    'separacion': 100,
    'margen': 100, #Debe ser mayor que LADO_CUADRADO
    'tamaño_letra': 15,
    'tamaño_letra_grande': 30,
    'ANCHO_VENTANA': 800,
    'ALTO_VENTANA': 600,
}
MEDIANO = {
    'LADO_CUADRADO': 40,
    'separacion': 120,
    'margen': 100, #Debe ser mayor que LADO_CUADRADO
    'tamaño_letra': 30,
    'tamaño_letra_grande': 60,
    'ANCHO_VENTANA': 1000,
    'ALTO_VENTANA': 800,
}
GRANDE = {
    'LADO_CUADRADO': 60,
    'separacion': 200,
    'margen': 200, #Debe ser mayor que LADO_CUADRADO
    'tamaño_letra': 40,
    'tamaño_letra_grande': 80,
    'ANCHO_VENTANA': 1500,
    'ALTO_VENTANA': 1000,
}
TAMAÑO = PEQUEÑO # valor por defecto
tamaños = {0:PEQUEÑO, 1:MEDIANO, 2:GRANDE}
velocidad_texto = 100
idiomas = [
    ('es', 'español'),
    ('en', 'english'),
    ('fr', 'français')
]
