# -*- coding: utf-8 -*-
texto_intro = '''Welcome to pyBattleship classic: The mission
-.-.-.-.-.-.-.-.-.-.
You must control Freedonia's Navy in order to 
defend your country from Sylvania's menace...
-.-.-.-.-.-.-.-.-.-.
First you have to place your boats:
 - right click to turn your boat
 - left click to place your boat
 - you can't put a boat out of the board or 
 put a boat next to another vertically,
 horizontally even diagonally 
 - press ENTER when you finish
You have to place:
''' 
texto_longitud_barco='one ship of length'
texto_configurar='Press F10 to configure language and screen resolution'
texto_continuar = '\nPress ENTER to continue'
texto_intro_jugar = '''Once you have placed your
boats you have to shoot, write the coordinates and 
press ENTER.
CAREFULL, you are fighting a ROBOTIC enemy!!!
'''
título = 'pyBattleship Classic'
texto_alerta_fuera='''You can't place a boat out of the board'''
texto_alerta_barcos_juntos = '''You can't place a boat next to another'''
texto_coordenadas_erroneas = '''You have to write a letter followed by a number'''
texto_alerta_sin_terminar = '''You haven't placed all your boats'''
texto_alerta_hundido = 'Sank boat'
texto_has_perdido = '''You have failed your mission 
just hope others are luckier than you!
'''
texto_has_ganado = ''''You gived a lesson to 
Sylvania's Navy! We hope others follow your steps
'''
texto_creditos = '''pyBattleship classic is a game created with Python and Pygame

by

César Angulo
and
Pablo Angulo

Hope you enjoyed this game 

(sound: morse-code-alphabet.ogg
https://freesound.org/people/
        Janosch-JR/sounds/479229/ )
'''
texto_volver_a_jugar = '''Press ENTER to play aigain,
ESCAPE to leave the game.

'''
texto_config_jugar = 'Play'
texto_config_idioma = 'Change Language'
texto_config_pantalla = 'Screen resolution'
texto_resolucion_peque = 'Small'
texto_resolucion_medio = 'Medium'
texto_resolucion_grande = 'Large'
