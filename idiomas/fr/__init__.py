# -*- coding: utf-8 -*-
texto_intro = '''Bienvenu à pyBattleship classic: Voici la mission
-.-.-.-.-.-.-.-.-.-.
Vous devez contrôler la flotte de Freedonia afin 
de protéger votre pays de la menace de Sylvania...
-.-.-.-.-.-.-.-.-.-.
Tout d'abord, placer les bateaux : 
 - clic droit pour tourner votre bateau
 - clic gauche pour placer votre bateau
 - vous ne pouvez pas placer un bateau en dehors du 
 tableau ou le coller à un autre bateau que ce soit  
 verticallement, horizontallement ou encore en diagonal 
 - appuyer sur ENTRÉE une fois terminé
Vous devez placer:
''' 
texto_longitud_barco='un bateau de longueur '
texto_configurar='''Appuyer sur F10 pour configurer le language et la résolution de l'écran'''
texto_continuar = '\nAppuyer sur ENTRÉE pour continuer'
texto_intro_jugar = '''Une fois vos bateaux placés, 
vous devez tirer, taper les coordonnés et appuyer sur ENTRÉE.

ATTENTION, vous êtes en train de combattre un ennemi ROBOTIQUE!!!
'''
título = 'pyBattleship Classic'
texto_alerta_fuera='''Vous ne pouvez pas placer un bateau en dehors du tableau'''
texto_alerta_barcos_juntos = '''Vous ne pouvez pas coller un bateau l'un à l'autre'''
texto_coordenadas_erroneas = '''Vous devez écrire une lettre suivie d'un chiffre'''
texto_alerta_sin_terminar = '''Vous n'avez pas placé tous vos bateaux'''
texto_alerta_hundido = 'Bateau coulé'
texto_has_perdido = '''Vous avez échoué votre mission 
espérons que d'autres sont plus chanceux que vous!
'''
texto_has_ganado = ''''Vous avez donné une leçon à  
la flotte de Sylvania! Nous espérons que d'autres vont suivre vos pas.
'''
texto_creditos = '''pyBattleship classic est un jeu développé sous Python et Pygame

par

César Angulo
et
Pablo Angulo

Nous espérons que vous avez appréciés ce jeu 

(son: morse-code-alphabet.ogg
https://freesound.org/people/
        Janosch-JR/sounds/479229/ )
'''
texto_volver_a_jugar = '''Appuyer sur Entrée pour rejouer ENTER,
ECHAP pour quitter le jeu.

'''
texto_config_jugar = 'Jouer'
texto_config_idioma = 'Changer language'
texto_config_pantalla = "Résolution de l'écran"
texto_resolucion_peque = 'Petit'
texto_resolucion_medio = 'Moyen'
texto_resolucion_grande = 'Grand'
