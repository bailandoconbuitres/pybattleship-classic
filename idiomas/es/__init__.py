# -*- coding: utf-8 -*-
texto_intro = '''Bienvenido a pyBarcos: la misión
-.-.-.-.-.-.-.-.-.-.
Te ha tocado controlar la marina de Libertonia y
defender a tu país de la agresión del vecino
Sylvania...
-.-.-.-.-.-.-.-.-.-.
Primero tienes que colocar tus barcos:
 - pincha con el botón derecho para girarlos
 - con el botón izquierdo para elegir la casilla
 - no puedes ni sacar un barco del tablero ni
 juntar dos barcos en vertical, en horizontal ni
 en diagonal
 - pulsa ENTER cuando hayas terminado
 Tienes que colocar:
'''
texto_longitud_barco='un barco de longitud'
texto_configurar='Pulsa F10 para configurar idioma y resolución de pantalla'
texto_continuar = '\nPulsa ENTER para continuar'
texto_intro_jugar = '''Ahora que has colocado los
barcos tienes que disparar, escribe las
coordenadas y pulsa ENTER.
¡¡¡CUIDADO, te enfrentas a un enemigo ROBÓTICO!!!
'''
texto_alerta_fuera='No puedes sacar un barco del tablero'
texto_alerta_barcos_juntos = 'No se puede colocar un barco al lado de otro'
texto_coordenadas_erroneas = 'Tienes que escribir una letra seguida de un número'
texto_alerta_sin_terminar = 'No has terminado de colocar los barcos'
texto_alerta_hundido = 'Barco hundido'
texto_has_perdido = '¡Has perdido la batalla esperamos qué otros tengan más suerte qué tú!'
texto_has_ganado = '''¡Has ganado, le diste una buena lección a la marina
de Silvania!
Ojalá los demás sigan tus pasos'''
título = 'pyBarcos'
texto_creditos = '''pyBarcos es un juego desarrollado con 
              Python y Pygame

por 

César Angulo
y
Pablo Angulo 

Esperamos que os haya gustado 

(sonido: morse-code-alphabet.ogg 
https://freesound.org/people/
        Janosch-JR/sounds/479229/ )
''' 
texto_volver_a_jugar = '''Pulsa Enter para volver a jugar,
Escape para salir

'''
texto_config_jugar = 'Jugar'
texto_config_idioma = 'Cambiar idioma'
texto_config_pantalla = 'Resolución de pantalla'
texto_resolucion_peque = 'Tamaño pequeño'
texto_resolucion_medio = 'Tamaño mediano'
texto_resolucion_grande = 'Tamaño grande'
