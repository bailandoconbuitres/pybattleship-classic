# -*- coding: utf-8 -*-
from random import *
import importlib

import pygame

from configuracion import *
from time import *

pygame.init()
clock = pygame.time.Clock()
pygame.font.init() # you have to call this at the start,
                   # if you want to use this module.
pygame.mixer.music.load('sonidos/morse-code-alphabet.ogg')
título = 'pyBattleship classic'
win = pygame.display.set_mode((TAMAÑO['ANCHO_VENTANA'], TAMAÑO['ALTO_VENTANA']))

def init_window():
    # Initialise display, set the caption and icon
    tamano_ventana_juego()
    win.fill(WHITE)
    pygame.display.set_caption(título)

def tamano_ventana_juego():
    global monospace_large, monospace_xxl
    pygame.display.set_mode((TAMAÑO['ANCHO_VENTANA'], TAMAÑO['ALTO_VENTANA']))
    monospace_large = pygame.font.SysFont('Monospace', TAMAÑO['tamaño_letra'])
    monospace_xxl = pygame.font.SysFont('Monospace Bold', TAMAÑO['tamaño_letra_grande'])

#    pygame.display.set_mode((TAMAÑO['separacion'] + TAMAÑO['LADO_CUADRADO']*8*2 +2*TAMAÑO['margen'], TAMAÑO['LADO_CUADRADO']*8 +2*TAMAÑO['margen']))
    win.fill(WHITE)

def configurar():
    eleccion = configuracion()
    while eleccion>0:
        if eleccion==1:
            idioma = elegir_idioma()
            cargar_idioma(idioma)
        elif eleccion==2:
            tamaño = elegir_resolucion_pantalla()
            cambiar_resolucion_pantalla(tamaño)
        eleccion = configuracion()

def trasponer(tablero):
    tablero_tras = [[' ']*lado for _ in range(lado)]
    for i in range(lado):
        for j in range(lado):
            tablero_tras[i][j] = tablero[j][i]
    return tablero_tras

def pprint(tablero):
    tablero_tras = trasponer(tablero)
    for fila in tablero_tras:
        print(fila)

def dibuja_cuadricula(x0, y0):
    pygame.draw.rect(win, WHITE,
                     (x0, y0,
                      lado*TAMAÑO['LADO_CUADRADO'], lado*TAMAÑO['LADO_CUADRADO']))
    #lineas horizontales
    for j in range(0,lado+1):
        pygame.draw.line(win, BLACK,
                         (x0, y0 + TAMAÑO['LADO_CUADRADO']*j),
                         (x0 + TAMAÑO['LADO_CUADRADO']*lado, y0 +TAMAÑO['LADO_CUADRADO']*j), 1)
    #letras en las filas
    for j in range(0,lado):
        letra = monospace_large.render(letras[j], True, BLACK)
        win.blit(letra, (x0-TAMAÑO['LADO_CUADRADO'], y0 + TAMAÑO['LADO_CUADRADO']*j))

    # lineas verticales
    for j in range(0,lado+1):
        pygame.draw.line(win, BLACK,
                         (x0 + TAMAÑO['LADO_CUADRADO']*j, y0),
                         (x0 + TAMAÑO['LADO_CUADRADO']*j, y0 +TAMAÑO['LADO_CUADRADO']*lado), 1)

    #numeros en las columnas
    for j in range(0,lado):
        letra = monospace_large.render(str(j+1), True, BLACK)
        win.blit(letra, (x0 + TAMAÑO['LADO_CUADRADO']*j + (TAMAÑO['LADO_CUADRADO'] - TAMAÑO['tamaño_letra']), y0 +TAMAÑO['LADO_CUADRADO']*lado))

def dibuja_un_tablero(x0,y0,tablero):
    dibuja_cuadricula(x0,y0)
    for fila in range(lado):
        for columna in range(lado):
            casilla = tablero[fila][columna]
            if casilla in 'BOX':
                if casilla=='B':
                    color = GREY
                elif casilla=='O':
                    color = BLUE
                elif casilla=='X':
                    color = RED
                pygame.draw.circle(win, color,
                             (x0+TAMAÑO['LADO_CUADRADO']*fila+TAMAÑO['LADO_CUADRADO']//2, y0 + TAMAÑO['LADO_CUADRADO']*columna+TAMAÑO['LADO_CUADRADO']//2),
                             TAMAÑO['LADO_CUADRADO']//2
                    )
            elif (casilla == ' ') or (casilla == '.'):
                pass
            else:
                letra = monospace_large.render(casilla, True, BLACK)
                win.blit(letra, (x0 + TAMAÑO['LADO_CUADRADO']*fila, y0 +TAMAÑO['LADO_CUADRADO']*columna))


def oculta_barcos(tablero):
    nuevo_tablero = []
    for fila in tablero:
        nueva_fila = [(' ' if casilla=='B' else casilla) for casilla in fila]
        nuevo_tablero.append(nueva_fila)
    return nuevo_tablero

def dibuja_tableros(tablero1, tablero2):
    #Tablero 1
    x0 = TAMAÑO['margen']
    y0 = TAMAÑO['margen']
    dibuja_un_tablero(x0,y0,tablero1)
    x0 = TAMAÑO['margen'] + TAMAÑO['separacion'] + TAMAÑO['LADO_CUADRADO']*lado
    y0 = TAMAÑO['margen']
    tablero2_oculto = oculta_barcos(tablero2)
    dibuja_un_tablero(x0,y0,tablero2_oculto)
    pygame.display.update()

def disparo_ordenador_tonto(tablero):
    'Ya no se usa'
    x = randint(0,lado-1)
    y = randint(0,lado-1)
    print('El ordenador dispara:', traducir_coordenadas_al_reves(x, y))
    return x,y

def disparo_ordenador_menos_tonto(tablero):
    'Ya no se usa'
    x = randint(0,lado-1)
    y = randint(0,lado-1)
    #Puede ser desconocido, o barco, porque el tablero llega ofuscado
    if tablero[x][y]==' ':
        print('El ordenador dispara:', traducir_coordenadas_al_reves(x, y))
    else:
        print('aquí ya he disparado, mejor vuelvo a lanzar los dados')
        x,y = disparo_ordenador_menos_tonto(tablero)
    return x,y

def vecinos_de(x,y):
    vecinos = []
    if x>0:
        vecinos.append((x-1,y))
    if x<lado-1:
        vecinos.append((x+1,y))
    if y>0:
        vecinos.append((x,y-1))
    if y<lado-1:
        vecinos.append((x,y+1))
    return vecinos

def algun_vecino_es_X(tablero, x, y):
    vecinos = vecinos_de(x,y)
    return any(tablero[x_vecino][y_vecino]=='X'
               for (x_vecino, y_vecino) in vecinos)

def disparo_ordenador_medio_listo(tablero):
    casillas_prioritarias = [
        (x,y) for x in range(lado)
              for y in range(lado)
        if (tablero[x][y]==' ') and algun_vecino_es_X(tablero, x,y)
        ]
    if len(casillas_prioritarias) > 0:
        x,y = choice(casillas_prioritarias)
    else:
        x,y = disparo_ordenador_menos_tonto(tablero)
    return x,y

def disparo(tablero, x, y):
    elemento_antiguo = tablero[x][y]
    if elemento_antiguo in ' .':
        elemento_nuevo = 'O'
    elif elemento_antiguo=='B':
        elemento_nuevo = 'X'
        if x>0 and y>0 and tablero[x-1][y-1] == ' ':
            tablero[x-1][y-1] = '.'
        if x>0 and y<lado-1 and tablero[x-1][y+1] == ' ':
            tablero[x-1][y+1] = '.'
        if x<lado-1 and y>0 and tablero[x+1][y-1] == ' ':
            tablero[x+1][y-1] = '.'
        if x<lado-1 and y<lado-1 and tablero[x+1][y+1] == ' ':
            tablero[x+1][y+1] = '.'
    else:
        elemento_nuevo = elemento_antiguo
    tablero[x][y] = elemento_nuevo
    return tablero


def disparo_viejo(tablero, x, y):
    nuevo_tablero = []
    for numero_fila in range(lado):
        if numero_fila != y:
            fila_antigua = tablero[numero_fila]
            nuevo_tablero.append(fila_antigua)
        else:
            fila_antigua = tablero[numero_fila]
            fila_nueva = []
            for numero_columna in range(lado):
                if numero_columna != x:
                    elemento_antiguo = fila_antigua[numero_columna]
                    fila_nueva.append(elemento_antiguo)
                else:
                    elemento_antiguo = fila_antigua[numero_columna]
                    if elemento_antiguo==' ':
                        elemento_nuevo = 'O'
                    elif elemento_antiguo=='B':
                        elemento_nuevo = 'X'
                    else:
                        elemento_nuevo = elemento_antiguo
                    fila_nueva.append(elemento_nuevo)
            nuevo_tablero.append(fila_nueva)
    if elemento_nuevo=='X':
        # marcamos con diagonales con punto
        pass
    return nuevo_tablero

def dibuja_barco_desde_arriba(largo, x, y, vertical, color=GREY):
    if vertical:
        pygame.draw.polygon(win, color, [
            (x, y  + TAMAÑO['LADO_CUADRADO']//2),
            (x, y + TAMAÑO['LADO_CUADRADO']//2 + TAMAÑO['LADO_CUADRADO']*(largo-1)),
            (x + TAMAÑO['LADO_CUADRADO']//2, y + TAMAÑO['LADO_CUADRADO']*largo),
            (x + TAMAÑO['LADO_CUADRADO'], y + TAMAÑO['LADO_CUADRADO']//2 + TAMAÑO['LADO_CUADRADO']*(largo-1)),
            (x + TAMAÑO['LADO_CUADRADO'], y  + TAMAÑO['LADO_CUADRADO']//2),
            (x + TAMAÑO['LADO_CUADRADO']//2, y)
            ])
    else:
        pygame.draw.polygon(win, color, [
            (x + TAMAÑO['LADO_CUADRADO']//2, y),
            (x + TAMAÑO['LADO_CUADRADO']//2 + TAMAÑO['LADO_CUADRADO']*(largo-1), y),
            (x + TAMAÑO['LADO_CUADRADO']*largo, y + TAMAÑO['LADO_CUADRADO']//2),
            (x + TAMAÑO['LADO_CUADRADO']//2 + TAMAÑO['LADO_CUADRADO']*(largo-1), y + TAMAÑO['LADO_CUADRADO']),
            (x + TAMAÑO['LADO_CUADRADO']//2, y + TAMAÑO['LADO_CUADRADO']),
            (x, y + TAMAÑO['LADO_CUADRADO']//2)
            ])

def dibuja_barcos_en_fila(x, y, barco_seleccionado, barcos_colocados):
    barco_en_columna = [-1]*(sum(tipos_barcos) + len(tipos_barcos))
    columna = 0
    for j,largo in enumerate(tipos_barcos):
        if j == barco_seleccionado:
            color = BLUE
        elif barcos_colocados[j]:
            color = GREEN
        else:
            color = GREY
        dibuja_barco_desde_arriba(largo, x + columna*TAMAÑO['LADO_CUADRADO'], y, False, color)
        barco_en_columna[columna:columna+largo] = [j]*largo
        columna = columna + largo + 1
    return barco_en_columna

def traducir_coordenadas(disparo):
    if len(disparo)<2:
        return -1, -1
    letra, numero = disparo[:2]
    x = ord(numero) - ord('1')
    y = ord(letra.upper())- ord('A')
    return x,y

def traducir_coordenadas_al_reves(x,y):
    numero = str(x + 1)
    letra = chr(ord('A') + y)
    disparo = letra + numero
    return disparo

def ha_terminado(tablero):
    for linea in tablero:
        for elemento in linea:
            if elemento=='B':
                return False
    return True

def tablero_vacio():
    return [[' ']*lado for _ in range(lado)]

def tablero_duda():
    return [[' ']*lado for _ in range(lado)]

def ordenador_coloca_un_barco(tablero, largo):
    buena_posicion = False
    while not buena_posicion:
        vertical = choice([True, False])
        if vertical:
           columna = randint(0, lado-1)
           fila = randint(0, lado-1-largo)
        else: # if horizontal
            columna = randint(0, lado-1-largo)
            fila = randint(0, lado-1)
        buena_posicion = se_puede_colocar(largo, fila, columna, vertical, tablero)
    nuevo_tablero = coloca_un_barco(tablero, fila, columna, largo, vertical)
    if vertical:
        posiciones_barco = [
            (columna, fila + j) for j in range(largo)
        ]
    else:
        posiciones_barco = [(columna+k, fila) for k in range(largo)]
    return nuevo_tablero, posiciones_barco

def ordenador_coloca_barcos(tipos_barcos):
    tablero = tablero_vacio()
    posiciones_barcos = {}
    for n,largo in enumerate(tipos_barcos):
        tablero, posiciones_barco = ordenador_coloca_un_barco(tablero, largo)
        posiciones_barcos[n] = posiciones_barco
    return tablero, posiciones_barcos

def texto_jugador(texto):
    wth = monospace_large.render(texto, True, BLACK)
    pygame.draw.rect(win, WHITE,
                     (TAMAÑO['margen'] + TAMAÑO['tamaño_letra'] + TAMAÑO['LADO_CUADRADO']*lado, TAMAÑO['margen'],
                      2*TAMAÑO['tamaño_letra'], TAMAÑO['tamaño_letra']))
    win.blit(wth, (TAMAÑO['margen'] + TAMAÑO['tamaño_letra'] + TAMAÑO['LADO_CUADRADO']*lado, TAMAÑO['margen']))

def texto_ordenador(texto):
    wth = monospace_large.render(texto, True, RED)
    pygame.draw.rect(win, WHITE,
                     (TAMAÑO['margen'] + TAMAÑO['tamaño_letra'] + TAMAÑO['LADO_CUADRADO']*lado, TAMAÑO['margen'] + TAMAÑO['LADO_CUADRADO'],
                      2*TAMAÑO['tamaño_letra'], TAMAÑO['tamaño_letra']))
    win.blit(wth, (TAMAÑO['margen'] + TAMAÑO['tamaño_letra'] + TAMAÑO['LADO_CUADRADO']*lado, TAMAÑO['margen'] + TAMAÑO['LADO_CUADRADO']))

def texto_victoria(texto):
    x = 50
    y = TAMAÑO['ALTO_VENTANA'] - TAMAÑO['tamaño_letra']*4
    dibuja_texto_largo(texto, x, y, monospace_large, TAMAÑO['tamaño_letra'])

def comprueba_hundido(tablero, posiciones_barcos, x, y):
    if tablero[x][y] != 'B':
        return False
    if not algun_vecino_es_X(tablero, x, y):
        return False
    for j, posiciones_barco in posiciones_barcos.items():
        if any( (xbarco==x ) and (ybarco==y)
                for xbarco, ybarco in posiciones_barco ):
            #estoy disparando al barco j
            daño = sum(1 for xbarco, ybarco in posiciones_barco
                       if tablero[xbarco][ybarco]=='X')
            largo = len(posiciones_barco)
            if daño == largo-1:
#                import pudb; pudb.set_trace()
                x_primer, y_primer = posiciones_barco[0]
                x_ultima, y_ultima = posiciones_barco[-1]
                if x_primer == x_ultima:
                    # vertical
                    # primera_casilla
                    x = x_primer
                    y = y_primer - 1
                    if y>=0 and tablero[x][y] == ' ':
                        tablero[x][y] = '.'
                    # ultima_casilla
                    x = x_primer
                    y = y_ultima + 1
                    if y<lado and tablero[x][y] == ' ':
                        tablero[x][y] = '.'
                else:
                    # horizontal
                    # primera_casilla
                    x = x_primer - 1
                    y = y_primer
                    if x>=0 and tablero[x][y] == ' ':
                        tablero[x][y] = '.'
                    # ultima_casilla
                    x = x_ultima + 1
                    y = y_ultima
                    if x<lado and tablero[x][y] == ' ':
                        tablero[x][y] = '.'
                alerta(texto_alerta_hundido)
                return True

def jugar(tablero1, tablero2, posiciones_barcos1, posiciones_barcos2):
    init_window()
    alerta('')
    pygame.mixer.music.stop()
    teclas = []
    seguir_jugando = True

    while seguir_jugando:
        dibuja_tableros(tablero1, tablero2)
        dibuja_alerta()
        sleep(0.1)
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN:
                if event.key==pygame.K_F10:
                    configurar()
                    init_window()
                    dibuja_tableros(tablero1, tablero2)
                teclas.append(event.key)
            if ((len(teclas)>=3) or
                (teclas and
                 (teclas[-1] in (pygame.K_RETURN, pygame.K_ESCAPE)))
                ):
                if (len(teclas)>=3) and (teclas[2]==pygame.K_RETURN):
                    coordenadas = chr(teclas[0]) + chr(teclas[1])

                    x,y = traducir_coordenadas(coordenadas)
                    disparo_ok = ((0<=x<lado) and
                                  (0<=y<lado) )
                else:
                    disparo_ok = False
                teclas = []

                if disparo_ok:
                    print()
                    pprint(tablero1)
                    print()
                    pprint(tablero2)
                    texto_jugador(coordenadas)
                    comprueba_hundido(tablero2, posiciones_barcos2, x, y)
                    tablero2 = disparo(tablero2, x, y)

                    if ha_terminado(tablero2):
                        dibuja_tableros(tablero1, tablero2)
                        texto_victoria(texto_has_ganado)
                        pygame.display.update()
                        sleep(3)
                        seguir_jugando = False
                    dibuja_tableros(tablero1, tablero2)
                    sleep(1)
                    x,y = disparo_ordenador_medio_listo(oculta_barcos(tablero1))
                    texto_ordenador(traducir_coordenadas_al_reves(x,y))
                    comprueba_hundido(tablero1, posiciones_barcos1, x, y)
                    tablero1 = disparo(tablero1, x, y)
                    if ha_terminado(tablero1):
                        texto_victoria(texto_has_perdido)
                        pygame.display.update()
                        sleep(3)
                        dibuja_tableros(tablero1, tablero2)
                        seguir_jugando = False
                else:
                    texto_jugador('??')
                    alerta(texto_coordenadas_erroneas)

def dibuja_texto_largo(texto, x, y, font, fontsize):
    lineas = texto.splitlines()
    pygame.draw.rect(win, WHITE,
                     (x, y, TAMAÑO['ANCHO_VENTANA'] - x, fontsize*len(lineas)))
    for i, l in enumerate(lineas):
        win.blit(font.render(l, True, BLACK), (x, y + fontsize*i))

def se_puede_colocar(largo, fila, columna, vertical, tablero):
    if (vertical and fila+largo>lado):
        alerta(texto_alerta_fuera)
        return False
    if (not vertical and columna+largo>lado):
        alerta(texto_alerta_fuera)
        return False
    if vertical:
        for y in range(max(0,fila-1), min(fila+largo+1, lado)):
            for x in range(max(0,columna-1), min(columna + 2, lado)):
                if tablero[x][y]=='B':
                    alerta(texto_alerta_barcos_juntos)
                    return False
    else:
        for y in range(max(0,fila-1), min(fila+2, lado)):
            for x in range(max(0,columna-1), min(columna + largo+1, lado)):
                if tablero[x][y]=='B':
                    alerta(texto_alerta_barcos_juntos)
                    return False
    return True

def coloca_un_barco(tablero, x, y, largo, vertical):
    if vertical:
        for j in range(largo):
            tablero = coloca_barcos(tablero, x + j, y)
    else:
        for j in range(largo):
            tablero = coloca_barcos(tablero, x, y + j)
    return tablero

def coloca_barcos(tablero, x, y):
    nuevo_tablero = []
    for numero_fila in range(lado):
        if numero_fila != y:
            fila_antigua = tablero[numero_fila]
            nuevo_tablero.append(fila_antigua)
        else:
            fila_antigua = tablero[numero_fila]
            fila_nueva = []
            for numero_columna in range(lado):
                if numero_columna != x:
                    elemento_antiguo = fila_antigua[numero_columna]
                    fila_nueva.append(elemento_antiguo)
                else:
                    elemento_antiguo = fila_antigua[numero_columna]
                    if elemento_antiguo==' ':
                        elemento_nuevo = 'B'
                    elif elemento_antiguo=='B':
                        elemento_nuevo = ' '
                    else:
                        elemento_nuevo = elemento_antiguo
                    fila_nueva.append(elemento_nuevo)
            nuevo_tablero.append(fila_nueva)
    return nuevo_tablero

def colocar_barcos_viejo():
    tablero1 = tablero_vacio()
    tablero2 = tablero_vacio()
    seguir_colocando = True
    while seguir_colocando:
        dibuja_tableros(tablero1, tablero2)
        sleep(0.1)
        events = pygame.event.get()
        # proceed events
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                seguir_colocando = False
            elif event.type == pygame.MOUSEBUTTONUP:
                x,y = pygame.mouse.get_pos()
                print(x,y)
                if ((TAMAÑO['margen'] <= x < TAMAÑO['margen'] + lado*TAMAÑO['LADO_CUADRADO']) and
                    (TAMAÑO['margen'] <= y < TAMAÑO['margen'] + lado*TAMAÑO['LADO_CUADRADO'])):
                    columna = (x - TAMAÑO['margen'])//TAMAÑO['LADO_CUADRADO']
                    fila = (y - TAMAÑO['margen'])//TAMAÑO['LADO_CUADRADO']
                    print(columna, fila)
                    print(traducir_coordenadas_al_reves(fila, columna))
                    tablero1 = coloca_barcos(tablero1, fila, columna)
    return tablero1

tiempo_ultima_alerta = pygame.time.get_ticks()
texto_alerta = ''

def alerta(texto):
    pygame.mixer.music.play()
    global texto_alerta, tiempo_ultima_alerta
    tiempo_ultima_alerta = pygame.time.get_ticks()
    texto_alerta = texto

def dibuja_alerta():
    hasta_aqui = int((pygame.time.get_ticks() - tiempo_ultima_alerta)/velocidad_texto)
    texto = texto_alerta[:hasta_aqui]
    if hasta_aqui>len(texto_alerta):
        pygame.mixer.music.stop()
    x_alerta = 50
    y_alerta = TAMAÑO['ALTO_VENTANA'] - 100
    pygame.draw.rect(win, WHITE,
                     (x_alerta, y_alerta, TAMAÑO['ANCHO_VENTANA'], TAMAÑO['LADO_CUADRADO']))
    win.blit(monospace_large.render(texto, True, BLACK), (x_alerta, y_alerta))

def colocar_barcos():
    init_window()
    num_barcos = len(tipos_barcos)
    posiciones_barcos = {j:[] for j in range(num_barcos)}
    tablero1 = tablero_vacio()
    tablero2 = tablero_duda()
    y_coloca_barcos_min = TAMAÑO['margen'] + TAMAÑO['LADO_CUADRADO']*(lado+2)
    y_coloca_barcos_max = y_coloca_barcos_min + TAMAÑO['LADO_CUADRADO']
    x_coloca_barcos_min = TAMAÑO['separacion']
    x_coloca_barcos_max = (x_coloca_barcos_min
        + sum(largo for largo in tipos_barcos)*TAMAÑO['LADO_CUADRADO']
        + sum(1 for largo in tipos_barcos)*TAMAÑO['LADO_CUADRADO']
    )
    seguir_colocando = True
    barco_seleccionado = -1
    barcos_colocados = [False]*len(tipos_barcos)
    vertical = False
    while seguir_colocando:
        sleep(0.1)
        dibuja_tableros(tablero1, tablero2)
        dibuja_alerta()
        barco_en_columna = dibuja_barcos_en_fila(
            x_coloca_barcos_min, y_coloca_barcos_min,
            barco_seleccionado, barcos_colocados)
        if ((barco_seleccionado >= 0) and
            (TAMAÑO['margen'] <= x < TAMAÑO['margen'] + lado*TAMAÑO['LADO_CUADRADO']) and
            (TAMAÑO['margen'] <= y < TAMAÑO['margen'] + lado*TAMAÑO['LADO_CUADRADO'])):
            columna = (x - TAMAÑO['margen'])//TAMAÑO['LADO_CUADRADO']
            fila = (y - TAMAÑO['margen'])//TAMAÑO['LADO_CUADRADO']
            largo = tipos_barcos[barco_seleccionado]
            if vertical and (fila+largo <= lado):
                dibuja_barco_desde_arriba(largo, TAMAÑO['margen']+columna*TAMAÑO['LADO_CUADRADO'], TAMAÑO['margen']+fila*TAMAÑO['LADO_CUADRADO'], vertical, color=GREY)
            elif (not vertical) and (columna + largo <= lado):
                dibuja_barco_desde_arriba(largo, TAMAÑO['margen']+columna*TAMAÑO['LADO_CUADRADO'], TAMAÑO['margen']+fila*TAMAÑO['LADO_CUADRADO'], vertical, color=GREY)
        pygame.display.update()
        events = pygame.event.get()
        # proceed events
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RETURN:
                if sum(barcos_colocados)<num_barcos:
                    alerta(texto_alerta_sin_terminar)
                else:
                    seguir_colocando = False
            elif event.type == pygame.MOUSEMOTION:
                x,y = pygame.mouse.get_pos()
            elif event.type == pygame.MOUSEBUTTONUP:
                x,y = pygame.mouse.get_pos()
                if event.button==3:
                    vertical = not vertical
                elif ((barco_seleccionado>=0) and
                      (TAMAÑO['margen'] <= x < TAMAÑO['margen'] + lado*TAMAÑO['LADO_CUADRADO']) and
                      (TAMAÑO['margen'] <= y < TAMAÑO['margen'] + lado*TAMAÑO['LADO_CUADRADO'])):
                    columna = (x - TAMAÑO['margen'])//TAMAÑO['LADO_CUADRADO']
                    fila = (y - TAMAÑO['margen'])//TAMAÑO['LADO_CUADRADO']
                    if se_puede_colocar(largo, fila, columna, vertical, tablero1):
                        tablero1 = coloca_un_barco(tablero1, fila, columna, largo, vertical)
                        barcos_colocados[barco_seleccionado] = True
                        if vertical:
                            posiciones_barcos[barco_seleccionado] = [
                                (columna, fila + j) for j in range(largo)
                            ]
                        else:
                            posiciones_barcos[barco_seleccionado] = [
                                (columna + j, fila) for j in range(largo)
                            ]
                        barco_seleccionado = -1
                elif ((x_coloca_barcos_min <= x < x_coloca_barcos_max) and
                    (y_coloca_barcos_min <= y < y_coloca_barcos_max)):
                    columna = (x - x_coloca_barcos_min)//TAMAÑO['LADO_CUADRADO']
                    barco_seleccionado = barco_en_columna[columna]
                    if barcos_colocados[barco_seleccionado]:
                        for columna, fila in posiciones_barcos[barco_seleccionado]:
                            tablero1[columna][fila] = ' '
                        barcos_colocados[barco_seleccionado] = False
    alerta('')
    pygame.mixer.music.stop()
    return tablero1, posiciones_barcos

def pantalla_texto(texto_total):
    pygame.mixer.music.play(-1)
    init_window()
    wth = monospace_xxl.render(título, True, BLACK)
    win.blit(wth, (TAMAÑO['ANCHO_VENTANA']/2 - wth.get_bounding_rect().width/2, 50))
    tiempo_inicial = pygame.time.get_ticks()
    while True:
        clock.tick(40)
        hasta_aqui = int((pygame.time.get_ticks() - tiempo_inicial)/velocidad_texto)
        if hasta_aqui>len(texto_total):
            pygame.mixer.music.stop()
        texto = texto_total[:hasta_aqui]
        dibuja_texto_largo(texto, 100, 200, monospace_large, TAMAÑO['tamaño_letra'])
        pygame.display.update()
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            elif (event.type == pygame.KEYDOWN and
                  event.key in (pygame.K_RETURN, pygame.K_ESCAPE)):
                pygame.mixer.music.stop()
                return event.key
            elif (event.type == pygame.KEYDOWN and
                  event.key in (pygame.K_SPACE, pygame.K_DOWN)):
                # un truco para que ponga todo el texto
                tiempo_inicial = -100000
                pygame.mixer.music.stop()

def intro():
    texto_colocar = '\n'.join(texto_longitud_barco+str(largo)
        for largo in tipos_barcos)
    texto_total = texto_intro + texto_colocar + texto_continuar
    pantalla_texto(texto_total)

def intro_jugar():
    texto_total = texto_intro_jugar + texto_continuar
    pantalla_texto(texto_total)

def creditos():
    texto_total = texto_creditos
    pantalla_texto(texto_total)

def elegir_opcion(opciones):
    init_window()
    wth = monospace_xxl.render(título, True, BLACK)
    win.blit(wth, (TAMAÑO['ANCHO_VENTANA']/2 - wth.get_bounding_rect().width/2, 50))
    xinicio, yinicio = 100, 200
    linea = 0
    coordenadas_opciones = {}
    espacio = TAMAÑO['tamaño_letra'] + 30
    margen_configuracion = 3
    for indice_opcion, opcion in opciones:
        texto_opcion = monospace_large.render(opcion, True, BLACK)
        r = texto_opcion.get_bounding_rect()
        xtexto = xinicio
        ytexto = yinicio + espacio*linea
        coordenadas_opcion = (
            xinicio + r.left - margen_configuracion,
            ytexto + r.top - margen_configuracion,
            r.width + 2*margen_configuracion,
            r.height + 2*margen_configuracion
        )
        coordenadas_opciones[indice_opcion] = coordenadas_opcion
        pygame.draw.rect(win, BLACK, coordenadas_opcion, 1)
        win.blit(texto_opcion, (xinicio, ytexto))
        linea = linea + 1
#    print(coordenadas_idiomas)

    pygame.display.update()
    while True:
        clock.tick(40)
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                exit()
            elif event.type == pygame.MOUSEBUTTONUP:
                x,y = pygame.mouse.get_pos()
                print(x,y)
                for opcion, coordenadas in coordenadas_opciones.items():
                    xopcion, yopcion, ancho, alto = coordenadas
                    if ((x > xopcion) and
                        (y > yopcion) and
                        (x < xopcion + ancho) and
                        (y < yopcion + alto)):
                        return opcion

def elegir_idioma():
    return elegir_opcion(idiomas)

def configuracion():
    opciones = [
        (0, texto_config_jugar), (1, texto_config_idioma), (2, texto_config_pantalla)
    ]
    return elegir_opcion(opciones)

def elegir_resolucion_pantalla():
    opciones = [
        (0, texto_resolucion_peque), 
        (1, texto_resolucion_medio), 
        (2, texto_resolucion_grande)
    ]
    return elegir_opcion(opciones)

def cambiar_resolucion_pantalla(indice_tamaño):
    global TAMAÑO
    TAMAÑO = tamaños[indice_tamaño]
    tamano_ventana_juego()

def cargar_idioma(idioma):
    mod_idiomas = importlib.import_module('idiomas.' + idioma)
    globals().update(mod_idiomas.__dict__)
    pygame.display.set_caption(título)

def volver_a_jugar():
    tecla = pantalla_texto(texto_volver_a_jugar)
    if tecla == pygame.K_RETURN:
        return True
    elif tecla == pygame.K_ESCAPE:
        return False
