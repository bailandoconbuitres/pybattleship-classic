# pyBattleship Classic

The real Battleship game:
  1. Place the ships using the mouse
  2. Shoot the enemy ships using coordinates
  
_Multilingual_: currently in Spanish, English and French. Ping if you want to translate it to other language!

In order to play:
- Install [pyGame](https://www.pygame.org/)
- Download all files
- run `python3 jugar.py`

![Sample Video](screenshots/pybattleship_classic_video.webm)
![intro](screenshots/intro_good.png)
![shooting phase](screenshots/pybatlleship clasic_good.png)

Built with [python 3](https://www.python.org/) and [pyGame](https://www.pygame.org/) by

 - César Angulo
 - Pablo Angulo 
 - @anto1ne

Hope you enjoy this game

- sound: [morse-code-alphabet.ogg](https://freesound.org/people/Janosch-JR/sounds/479229/)
